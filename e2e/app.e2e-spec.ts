import { WorkshopBlogPage } from './app.po';

describe('workshop-blog App', () => {
  let page: WorkshopBlogPage;

  beforeEach(() => {
    page = new WorkshopBlogPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
